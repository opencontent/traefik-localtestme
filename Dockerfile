FROM traefik:1.7.26-alpine

COPY traefik.toml /etc/traefik.toml
COPY localtest.me.* /ssl/


CMD [ "-c", "/etc/traefik.toml" ]


